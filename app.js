
var express = require('express');
var gzip = require('compression');
var bodyparser = require('body-parser');
var app = express();
var port = process.env.PORT || 3000;

var reqlogger = require('./utils/logger.js');
var validator = require('./utils/validate.js');

app.use(bodyparser.json());
app.use(gzip());
app.use(reqlogger);
app.use(validator.validate_api_key);
app.use(validator.validate_content_type);
app.use(express.static('public'));


var users_v1 = require('./routes/v1/users.js');
var tracker_v1 = require('./routes/v1/tracker.js');
var images_v1 = require('./routes/v1/images.js');
var users_v2 = require('./routes/v2/users.js');

app.use('/api/v1/users',users_v1);
app.use('/api/v1/tracker',tracker_v1);
app.use('/api/v1/images',images_v1);
app.use('/api/v2/users',users_v2);

var noRouter = function(req,res,next) {
  res.status(404).send('Route '+req.path+' not found');
}

app.use(noRouter);

app.use(function(err, req, res, next) {
  console.error(err.code);
  console.error(err.stack);

  if(err.code == 1001) {
    res.status(403).json({error:{msg:err.message}});
  }else if(err.code == 1003) {
    res.status(403).json({error:{msg:err.message}});
  }else {
    res.status(500).json({error:{msg:'an unexpected error has occured'}});
  }

});

app.listen(port,function() {
  console.log('server is listening at ');
});
