/**
* @name images api - v1
*/
var express = require('express');
var router = express.Router();
var path = require('path');

router.get('/:name',function(req,resp) {
  console.log('get: params '+req.params.name);
  console.log('get: query '+req.query.apikey);
  if(req.query.apikey ==1234) {
    resp.sendFile(path.join(__dirname, '../public/images/', req.params.name+'.jpg'));
  }else {
    resp.status(403).json({error:{name:'you cannot access this file'}});
  }

});

module.exports = router;
