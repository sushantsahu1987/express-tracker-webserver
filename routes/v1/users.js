/**
* @name user api - v1
*/

var express = require('express');
var router = express.Router();

router.get('/',function(req,resp) {
  console.log('get: users');
  resp.json({fname:'sushant',lname:'sahu'});
});

/*
router.post('/',function(req,resp) {
  console.log('post: users');
  console.log('content type : '+req.get('Content-Type'));

  if("name" in req.body) {
    console.log('body '+req.body.name);
  }
  resp.json({response:{message:'success'}});
});
*/

module.exports = router;
