var df = require('dateformat');

var logger = function(req,resp,next) {
  console.log('base url '+req.path+' '+df(Date.now(),'h:MM:ss TT dd/mm/yyyy'));
  next();
}


module.exports = logger;
