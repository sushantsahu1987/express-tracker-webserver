var validate_content_type = function(req,resp,next) {

  if(req.get('Content-Type')=='application/json'){
    console.log('validate_content_type');
    next();
  }else {
    var err = new Error('Content-Type not supported');
    var codes = require('./constants.js');
    err.code = codes.INVALID_CONTENT_TYPE;
    next(err);
  }

}

var validate_api_key = function(req,resp,next) {

  if(req.query.api_key==123){
    console.log('validate.js');
    next();
  }else {
    var err = new Error('Invalid api key');
    var codes = require('./constants.js');
    err.code = codes.INVALID_API_KEY;
    next(err);
  }

}

module.exports ={
  validate_content_type: validate_content_type,
  validate_api_key: validate_api_key

};
